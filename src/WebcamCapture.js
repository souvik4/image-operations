import Webcam from "react-webcam";
import { useCallback, useRef, useState } from "react";


    const videoConstraints = {
        width: 220,
        height: 200,
        facingMode: "user"
    };
  
  export const WebcamCapture = () => {
    const [image,setImage]=useState('');
    
    const webcamRef = useRef(null);
  
    const capture = useCallback(
      () => {
        const imageSrc = webcamRef.current.getScreenshot();
        console.log(imageSrc)
        setImage(imageSrc)
      },
  
      [webcamRef]
    );
  
    return (
      <div style={{margin: '50px'}}>
        {image===''? <Webcam
          audio={false}
          height={200}
          ref={webcamRef}
          screenshotFormat="image/jpeg"
          width={220}
          videoConstraints={videoConstraints}
        />:<img src={image} alt=''/>}
        <div>
        <button 
        onClick={(e)=>{
            e.preventDefault();capture()}}>
        Capture</button>
        </div>
      </div>
    );
  };
