import { useState } from 'react';
import ReactCrop from 'react-image-crop';
import { WebcamCapture } from './WebcamCapture';


// function Crop() {
    
//     const [src, setSrc] = useState(null);
//     const [crop, setCrop] = useState({ aspect: 16 / 9 });
//     const [image, setImage] = useState(null);
//     const [output, setOutput] = useState(null);

//     const selectImage = (file) => {
//         setSrc(URL.createObjectURL(file));
//     };

// const cropImageNow = () => {
// 	const canvas = document.createElement('canvas');
// 	const scaleX = image.naturalWidth / image.width;
// 	const scaleY = image.naturalHeight / image.height;
// 	canvas.width = crop.width;
// 	canvas.height = crop.height;
// 	const ctx = canvas.getContext('2d');

// 	const pixelRatio = window.devicePixelRatio;
// 	canvas.width = crop.width * pixelRatio;
// 	canvas.height = crop.height * pixelRatio;
// 	ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
// 	ctx.imageSmoothingQuality = 'high';

// 	ctx.drawImage(
// 	image,
// 	crop.x * scaleX,
// 	crop.y * scaleY,
// 	crop.width * scaleX,
// 	crop.height * scaleY,
// 	0,
// 	0,
// 	crop.width,
// 	crop.height,
// 	);
	
// 	// Converting to base64
// 	const base64Image = canvas.toDataURL('image/jpeg');
// 	setOutput(base64Image);
// };

// return (
// 	<div className="App">
// 	<center>
// 		<input
// 		type="file"
// 		onChange={(e) => {
// 			selectImage(e.target.files[0]);
// 		}}
// 		/>
//     <img src={src} alt='' />
// 		<br />
// 		<br />
// 		<div>
// 		{src && (
// 			<div>
// 			<ReactCrop src={src} onImageLoaded={setImage} crop={crop} onChange={setCrop} />
// 			<br />
// 			<button onClick={cropImageNow}>Crop</button>
// 			<br />
// 			<br />
// 			</div>
// 		)}
// 		</div>
// 		<div>{output && <img src={output} alt=''/>}</div>
// 	</center>
//     <WebcamCapture />
// 	</div>
// );
// }

function Crop () {
    const [srcImg, setSrcImg] = useState(null);
    const [image, setImage] = useState(null);
    const [crop, setCrop] = useState({aspect: 16 / 9});
    const [result, setResult] = useState(null);

    const handleImage = async (event) => {
        setSrcImg(URL.createObjectURL(event.target.files[0]));
        console.log(event.target.files[0]);
    };

    const getCroppedImg = async () => {
        try {
            const canvas = document.createElement("canvas");
            const scaleX = image.naturalWidth / image.width;
            const scaleY = image.naturalHeight / image.height;
            canvas.width = crop.width;
            canvas.height = crop.height;
            const ctx = canvas.getContext("2d");
            ctx.drawImage(
                image,
                crop.x * scaleX,
                crop.y * scaleY,
                crop.width * scaleX,
                crop.height * scaleY,
                0,
                0,
                crop.width,
                crop.height
            );

            const base64Image = canvas.toDataURL("image/jpeg", 1);
            setResult(base64Image);
            console.log(result);
        } catch (e) {
            console.log("crop the image");
        }
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log(result);
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                    <h2>Select Image you want to crop</h2>
                    <div>
                        <input
                            type="file"
                            // accept="image/*"
                            onChange={handleImage}
                        />
                        <img src={srcImg} alt=''/>
                    </div>
                    <div>
                        {srcImg && (
                            <div>
                                <ReactCrop
                                    style={{maxWidth: "50%"}}
                                    src={srcImg}
                                    onImageLoaded={setImage}
                                    crop={crop}
                                    onChange={setCrop}
                                />
                                <button onClick={getCroppedImg}
                                >
                                    crop
                                </button>
                            </div>
                        )}
                        {result && (
                            <div>
                                <img src={result} alt='Cropped Img'/>
                            </div>
                        )}
                    </div>

                <button type="submit">
                    Submit
                </button>
            </form>

            <WebcamCapture />
        </div>
    );
}

export default Crop;